# Conway's Game Of Life

My take on Conway's game of life in Go. This project is a simple implementation to play with Go and Ebiten.

## 🧐 Features

- Implementation in Go
- GUI made with [Ebiten](https://github.com/hajimehoshi/ebiten)
- Random generation of map
- Possibility to load a map described in [Plaintext](https://conwaylife.com/wiki/Plaintext). See [./maps]() for examples.

## 🛠️ Install

```bash
go install gitlab.com/osechet/game-of-life@latest
```

## 🧑🏻‍💻 Usage

```bash
# Run with a random map
game-of-life -width 100 -height 100
# Run with a specific seed
game-of-life -width 100 -height 100 -seed 123456789
# Load a map file
game-of-life -width 100 -height 100 -map ./maps/gosper_glider_gun.cells
```

## ➤ License
Distributed under the MIT License. See [LICENSE](LICENSE) for more information.
