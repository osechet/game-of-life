FROM ghcr.io/goreleaser/goreleaser-cross:v1.21.6

RUN apt update && apt install -y \
    libc6-dev \
    libgl1-mesa-dev \
    libxcursor-dev \
    libxi-dev \
    libxinerama-dev \
    libxrandr-dev \
    libxxf86vm-dev \
    libasound2-dev \
    pkg-config \
  && rm -rf /var/lib/apt/lists/* /var/cache/apt/archives/*
