package game

import (
	"image/color"

	"github.com/hajimehoshi/ebiten/v2"
	"gitlab.com/osechet/game-of-life/life"
)

const (
	cellSize   = 5
	cellMargin = 1
)

var (
	cellImage = ebiten.NewImage(cellSize, cellSize)
)

// Board represents the game board.
type Board struct {
	grid *life.Grid
}

// NewBoard generates a new Board with giving a size.
func NewBoard(grid *life.Grid) (*Board, error) {
	b := &Board{
		grid: grid,
	}
	return b, nil
}

// Size returns the board size.
func (b *Board) Size() (int, int) {
	x := b.grid.Width*cellSize + (b.grid.Height+1)*cellMargin
	y := b.grid.Height*cellSize + (b.grid.Height+1)*cellMargin
	return x, y
}

// Update updates the board state.
func (b *Board) Update() error {
	b.grid.NextTurn()
	return nil
}

// Draw draws the board to the given boardImage.
func (b *Board) Draw(boardImage *ebiten.Image) {
	boardImage.Fill(frameColor)
	cellImage.Fill(color.White)
	for j := 0; j < b.grid.Height; j++ {
		for i := 0; i < b.grid.Width; i++ {
			op := &ebiten.DrawImageOptions{}
			x := i*cellSize + (i+1)*cellMargin
			y := j*cellSize + (j+1)*cellMargin
			op.GeoM.Translate(float64(x), float64(y))
			op.ColorScale.ScaleWithColor(cellBackgroundColor(b.grid.IsAlive(i, j)))
			boardImage.DrawImage(cellImage, op)
		}
	}
}
