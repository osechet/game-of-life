package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"runtime"
	"runtime/pprof"

	"github.com/hajimehoshi/ebiten/v2"

	"gitlab.com/osechet/game-of-life/game"
	"gitlab.com/osechet/game-of-life/life"
)

// Set when building with ldflags
var (
	version = "<not set>"
	commit  = "<not set>"
	date    = "<not set>"
)

// Flags
var (
	showVersion bool
	width       int
	height      int
	seed        int64
	mapFile     string
	cpuprofile  string
	memprofile  string
)

func run() error {
	var grid *life.Grid
	if len(mapFile) > 0 {
		f, err := os.Open(mapFile)
		if err != nil {
			return err
		}
		defer f.Close()
		grid, err = life.LoadGrid(width, height, f)
		if err != nil {
			return err
		}
	} else {
		grid = life.NewRandomGrid(width, height, seed)
	}

	g, err := game.New(grid)
	if err != nil {
		return err
	}
	ebiten.SetWindowSize(game.ScreenWidth, game.ScreenHeight)
	ebiten.SetWindowTitle("Game Of Life")
	if err := ebiten.RunGame(g); err != nil {
		return err
	}
	return nil
}

func main() {
	flag.BoolVar(&showVersion, "version", false, "print version information")
	flag.IntVar(&width, "width", 100, "the board's width")
	flag.IntVar(&height, "height", 100, "the board's height")
	flag.Int64Var(&seed, "seed", 0, "the random generator seed")
	flag.StringVar(&mapFile, "map", "", "the map file to load")
	flag.StringVar(&cpuprofile, "cpuprofile", "", "write cpu profile to `file`")
	flag.StringVar(&memprofile, "memprofile", "", "write memory profile to `file`")
	flag.Parse()

	if showVersion {
		fmt.Printf("%s - %s - %s\n", version, commit, date)
		return
	}

	if len(cpuprofile) != 0 {
		f, err := os.Create(cpuprofile)
		if err != nil {
			log.Fatal("could not create CPU profile: ", err)
		}
		defer f.Close() // error handling omitted for example
		if err := pprof.StartCPUProfile(f); err != nil {
			log.Fatal("could not start CPU profile: ", err)
		}
		defer pprof.StopCPUProfile()
	}

	err := run()
	if err != nil {
		log.Fatal(err)
	}

	if len(memprofile) != 0 {
		f, err := os.Create(memprofile)
		if err != nil {
			log.Fatal("could not create memory profile: ", err)
		}
		defer f.Close() // error handling omitted for example
		runtime.GC()    // get up-to-date statistics
		if err := pprof.WriteHeapProfile(f); err != nil {
			log.Fatal("could not write memory profile: ", err)
		}
	}
}
