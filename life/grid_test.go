package life

import (
	"errors"
	"io"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewRandomGrid(t *testing.T) {
	assert.NotNil(t, NewRandomGrid(3, 9, 0))
	assert.NotNil(t, NewRandomGrid(7, 4, 12345))
}

func TestLoadGrid(t *testing.T) {
	type args struct {
		w      int
		h      int
		reader io.Reader
	}
	tests := []struct {
		name      string
		args      args
		want      *Grid
		assertion assert.ErrorAssertionFunc
	}{
		{"read error", args{10, 5, mockReader{err: errors.New("read error")}}, nil, assert.Error},
		{"no error", args{10, 5, strings.NewReader("!Name: Test\n!\n...OOO...\n\n....O.....")}, &Grid{
			Width: 10, Height: 5,
			cells: [][]int{
				{0, 0, 0, 1, 1, 1, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 1, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
			},
		}, assert.NoError},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := LoadGrid(tt.args.w, tt.args.h, tt.args.reader)
			tt.assertion(t, err)
			assert.Equal(t, tt.want, got)
		})
	}
}

func TestGrid_IsAlive(t *testing.T) {
	type args struct {
		x int
		y int
	}
	tests := []struct {
		name string
		grid Grid
		args args
		want bool
	}{
		{"0,0 dead", Grid{Width: 3, Height: 3, cells: [][]int{{0, 1, 1}, {1, 1, 1}, {1, 1, 1}}}, args{0, 0}, false},
		{"0,0 alive", Grid{Width: 3, Height: 3, cells: [][]int{{1, 0, 0}, {0, 0, 0}, {0, 0, 0}}}, args{0, 0}, true},
		{"1,2 dead", Grid{Width: 3, Height: 3, cells: [][]int{{1, 1, 1}, {1, 1, 1}, {1, 0, 1}}}, args{1, 2}, false},
		{"1,2 alive", Grid{Width: 3, Height: 3, cells: [][]int{{0, 0, 0}, {0, 0, 0}, {0, 1, 0}}}, args{1, 2}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.want, tt.grid.IsAlive(tt.args.x, tt.args.y))
		})
	}
}

func TestGrid_NextTurn(t *testing.T) {
	tests := []struct {
		name string
		grid *Grid
		want *Grid
	}{
		{
			name: "empty",
			grid: &Grid{
				Width:  10,
				Height: 10,
				cells: [][]int{
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				},
			},
			want: &Grid{
				Width:  10,
				Height: 10,
				cells: [][]int{
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				},
			},
		},
		{
			name: "alone",
			grid: &Grid{
				Width:  10,
				Height: 10,
				cells: [][]int{
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				},
			},
			want: &Grid{
				Width:  10,
				Height: 10,
				cells: [][]int{
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				},
			},
		},
		{
			name: "crowded",
			grid: &Grid{
				Width:  10,
				Height: 10,
				cells: [][]int{
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 1, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 1, 1, 0, 0, 0, 0, 0, 0},
					{0, 0, 1, 1, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				},
			},
			want: &Grid{
				Width:  10,
				Height: 10,
				cells: [][]int{
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 1, 1, 0, 0, 0, 0, 0, 0},
					{0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 1, 1, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				},
			},
		},
		{
			name: "block",
			grid: &Grid{
				Width:  10,
				Height: 10,
				cells: [][]int{
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 1, 1, 0, 0, 0, 0},
					{0, 0, 0, 0, 1, 1, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				},
			},
			want: &Grid{
				Width:  10,
				Height: 10,
				cells: [][]int{
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 1, 1, 0, 0, 0, 0},
					{0, 0, 0, 0, 1, 1, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				},
			},
		},
		{
			name: "bee hive",
			grid: &Grid{
				Width:  10,
				Height: 10,
				cells: [][]int{
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 1, 1, 0, 0, 0, 0},
					{0, 0, 0, 1, 0, 0, 1, 0, 0, 0},
					{0, 0, 0, 0, 1, 1, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				},
			},
			want: &Grid{
				Width:  10,
				Height: 10,
				cells: [][]int{
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 1, 1, 0, 0, 0, 0},
					{0, 0, 0, 1, 0, 0, 1, 0, 0, 0},
					{0, 0, 0, 0, 1, 1, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				},
			},
		},
		{
			name: "loaf",
			grid: &Grid{
				Width:  10,
				Height: 10,
				cells: [][]int{
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 1, 1, 0, 0, 0, 0},
					{0, 0, 0, 1, 0, 0, 1, 0, 0, 0},
					{0, 0, 0, 0, 1, 0, 1, 0, 0, 0},
					{0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				},
			},
			want: &Grid{
				Width:  10,
				Height: 10,
				cells: [][]int{
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 1, 1, 0, 0, 0, 0},
					{0, 0, 0, 1, 0, 0, 1, 0, 0, 0},
					{0, 0, 0, 0, 1, 0, 1, 0, 0, 0},
					{0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				},
			},
		},
		{
			name: "boat",
			grid: &Grid{
				Width:  10,
				Height: 10,
				cells: [][]int{
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 1, 1, 0, 0, 0, 0},
					{0, 0, 0, 0, 1, 0, 1, 0, 0, 0},
					{0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				},
			},
			want: &Grid{
				Width:  10,
				Height: 10,
				cells: [][]int{
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 1, 1, 0, 0, 0, 0},
					{0, 0, 0, 0, 1, 0, 1, 0, 0, 0},
					{0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				},
			},
		},
		{
			name: "tub",
			grid: &Grid{
				Width:  10,
				Height: 10,
				cells: [][]int{
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
					{0, 0, 0, 0, 1, 0, 1, 0, 0, 0},
					{0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				},
			},
			want: &Grid{
				Width:  10,
				Height: 10,
				cells: [][]int{
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
					{0, 0, 0, 0, 1, 0, 1, 0, 0, 0},
					{0, 0, 0, 0, 0, 1, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
					{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.grid.NextTurn()
			assert.Equal(t, tt.want, tt.grid)
		})
	}
}

func TestGrid_countNeighbours(t *testing.T) {
	type args struct {
		cells [][]int
		x     int
		y     int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{"Corner TL - Min", args{[][]int{
			{StateAlive, StateDead, StateAlive},
			{StateDead, StateDead, StateAlive},
			{StateAlive, StateAlive, StateAlive},
		}, 0, 0}, 0},
		{"Corner TL - Max", args{[][]int{
			{StateAlive, StateAlive, StateAlive},
			{StateAlive, StateAlive, StateAlive},
			{StateAlive, StateAlive, StateAlive},
		}, 0, 0}, 3},
		{"Middle - Min", args{[][]int{
			{StateDead, StateDead, StateDead},
			{StateDead, StateAlive, StateDead},
			{StateDead, StateDead, StateDead},
		}, 1, 1}, 0},
		{"Middle - Max", args{[][]int{
			{StateAlive, StateAlive, StateAlive},
			{StateAlive, StateAlive, StateAlive},
			{StateAlive, StateAlive, StateAlive},
		}, 1, 1}, 8},
		{"Corner BL - Min", args{[][]int{
			{StateAlive, StateAlive, StateAlive},
			{StateDead, StateDead, StateAlive},
			{StateAlive, StateDead, StateAlive},
		}, 0, 2}, 0},
		{"Corner BL - Max", args{[][]int{
			{StateAlive, StateAlive, StateAlive},
			{StateAlive, StateAlive, StateAlive},
			{StateAlive, StateAlive, StateAlive},
		}, 0, 2}, 3},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.want, countNeighbours(tt.args.cells, tt.args.x, tt.args.y))
		})
	}
}

func Test_calculateNextState(t *testing.T) {
	type args struct {
		cells [][]int
		x     int
		y     int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{"Dead -> Dead", args{[][]int{
			{StateDead, StateDead, StateDead},
			{StateDead, StateDead, StateDead},
			{StateDead, StateDead, StateDead},
		}, 1, 1}, StateDead},
		{"Dead -> Alive", args{[][]int{
			{StateAlive, StateAlive, StateAlive},
			{StateDead, StateDead, StateDead},
			{StateDead, StateDead, StateDead},
		}, 1, 1}, StateAlive},
		{"Alive - Underpopulation", args{[][]int{
			{StateDead, StateDead, StateDead},
			{StateDead, StateAlive, StateDead},
			{StateDead, StateDead, StateDead},
		}, 1, 1}, StateDead},
		{"Alive - OK", args{[][]int{
			{StateAlive, StateDead, StateDead},
			{StateDead, StateAlive, StateAlive},
			{StateDead, StateDead, StateDead},
		}, 1, 1}, StateAlive},
		{"Alive - Overpopulation", args{[][]int{
			{StateAlive, StateAlive, StateAlive},
			{StateDead, StateAlive, StateDead},
			{StateDead, StateAlive, StateDead},
		}, 1, 1}, StateDead},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.want, calculateNextState(tt.args.cells, tt.args.x, tt.args.y))
		})
	}
}

type mockReader struct {
	data []byte
	err  error
}

func (r mockReader) Read(buf []byte) (int, error) {
	if r.err != nil {
		return -1, r.err
	}
	return copy(buf, r.data), nil
}
