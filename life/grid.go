package life

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"math/rand"
	"strings"
	"sync"
	"time"
)

const (
	StateDead int = iota
	StateAlive
)

type Grid struct {
	Width  int
	Height int
	cells  [][]int
}

func NewRandomGrid(w, h int, seed int64) *Grid {
	grid := &Grid{}
	grid.generate(w, h, seed)
	return grid
}

func LoadGrid(w, h int, reader io.Reader) (*Grid, error) {
	grid := &Grid{}
	err := grid.load(w, h, reader)
	if err != nil {
		return nil, err
	}
	return grid, nil
}

func (grid *Grid) generate(w, h int, seed int64) {
	if seed == 0 {
		seed = time.Now().UnixMicro()
	}
	log.Printf("Generating map with seed %d...", seed)
	rd := rand.New(rand.NewSource(seed))

	grid.Width = w
	grid.Height = h
	grid.cells = make([][]int, grid.Height)
	for y := 0; y < grid.Height; y++ {
		grid.cells[y] = make([]int, grid.Width)
		for x := 0; x < grid.Width; x++ {
			if rd.Float32() < 0.5 {
				grid.cells[y][x] = StateDead
			} else {
				grid.cells[y][x] = StateAlive
			}
		}
	}
}

func (grid *Grid) load(w, h int, reader io.Reader) error {
	log.Printf("Loading map...")
	grid.Width = w
	grid.Height = h
	grid.cells = make([][]int, grid.Height)

	var y int
	scanner := bufio.NewScanner(reader)
	for scanner.Scan() {
		grid.cells[y] = make([]int, grid.Width)
		line := scanner.Text()
		if strings.HasPrefix(line, "!") {
			// skip
			continue
		}
		for x, c := range line {
			if c == 'O' || c == '1' {
				grid.cells[y][x] = StateAlive
			}
		}
		y++
	}
	if err := scanner.Err(); err != nil {
		return fmt.Errorf("failed to read map file: %w", err)
	}
	for j := y; j < grid.Height; j++ {
		grid.cells[j] = make([]int, grid.Width)
	}
	return nil
}

func (grid Grid) IsAlive(x, y int) bool {
	return grid.cells[y][x] == StateAlive
}

func (grid *Grid) NextTurn() {
	cells := make([][]int, grid.Height)
	for y := 0; y < grid.Height; y++ {
		cells[y] = make([]int, grid.Width)
	}

	wg := sync.WaitGroup{}
	for y := 0; y < grid.Height; y++ {
		wg.Add(1)
		go func(y int) {
			for x := 0; x < grid.Width; x++ {
				cells[y][x] = calculateNextState(grid.cells, x, y)
			}
			wg.Done()
		}(y)
	}
	wg.Wait()
	grid.cells = cells
}

func calculateNextState(cells [][]int, x, y int) int {
	neighbours := countNeighbours(cells, x, y)
	if cells[y][x] == StateAlive {
		if neighbours < 2 {
			return StateDead
		} else if neighbours > 3 {
			return StateDead
		} else {
			return StateAlive
		}
	} else {
		if neighbours == 3 {
			return StateAlive
		} else {
			return StateDead
		}
	}
}

func countNeighbours(cells [][]int, x, y int) int {
	count := 0
	var minX, maxX, minY, maxY int
	if x == 0 {
		minX = 0
	} else {
		minX = x - 1
	}
	if x == len(cells[0])-1 {
		maxX = len(cells[0]) - 1
	} else {
		maxX = x + 1
	}
	if y == 0 {
		minY = 0
	} else {
		minY = y - 1
	}
	if y == len(cells)-1 {
		maxY = len(cells) - 1
	} else {
		maxY = y + 1
	}
	for j := minY; j <= maxY; j++ {
		for i := minX; i <= maxX; i++ {
			if i == x && j == y {
				continue
			}
			if cells[j][i] == StateAlive {
				count++
			}
		}
	}
	return count
}
